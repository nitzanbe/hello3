import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
   
  
  url =  'https://2qgisk99qa.execute-api.us-east-1.amazonaws.com/test';

  predict(years: number, incame: number):Observable<any>{
    let json = {
      "data": 
        {
          "years": years,
          "incame": incame
        }
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        return res.body;       
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
