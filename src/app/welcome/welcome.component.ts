import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
 
  email:string;
  users$;
  
  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.users$ = this.authService.getUser();
    this.users$.subscribe(
      data => {
        this.email = data.email;
      }
    )
  }

}
