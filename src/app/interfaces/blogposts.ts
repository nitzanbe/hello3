export interface Blogposts {
    id: number,
    title: string,
    body: string, 
    ST?: number 
}
