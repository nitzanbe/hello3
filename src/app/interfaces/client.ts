export interface Client {
    id:string,
    name: string,
    years: number,
    incame: number,
    category?: string,
    ST?: number
}