import { Component, OnInit } from '@angular/core';
import { SavesService } from '../saves.service';
import { AuthService } from './../auth.service';


@Component({
  selector: 'saves',
  templateUrl: './saves.component.html',
  styleUrls: ['./saves.component.css']
})
export class SavesComponent implements OnInit {

  saves$;
  panelOpenState = false;
  userId:string;

  constructor(public authService:AuthService, private savesService:SavesService) { }

  public deleteSave(id:string){
    this.savesService.deleteSave(this.userId,id);
  }

  like(id:string,ST:number){
    ST=ST+1;
    this.savesService.like(this.userId ,id ,ST);
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.saves$ = this.savesService.getSaves(this.userId);
      }
    )
  }


}
