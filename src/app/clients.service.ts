import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  
  clientCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getClients(userId){
    this.clientCollection = this.db.collection(`users/${userId}/clients`);
    return this.clientCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  public deleteClient(userId:string , id:string){
    this.db.doc(`users/${userId}/clients/${id}`).delete();
  }
  
  updateClient(userId:string,id:string,name:string,years:number,incame:number){
    this.db.doc(`users/${userId}/clients/${id}`).update(
      {
        name:name,
        years:years,
        incame:incame
      }
    )
  }

  save(userId:string,id:string,name:string,years:number,incame:number,category:string,ST:number){
    this.db.doc(`users/${userId}/clients/${id}`).update(
      {
        name:name,
        years:years,
        incame:incame,
        category:category,
        ST:ST
      }
    )
  }
  addClient(userId:string,name:string,years:number,incame:number,category:string,ST:number){
    const client = {name:name, years:years, incame:incame, category:category,ST:ST}; 
    this.userCollection.doc(userId).collection('clients').add(client);
  }



  constructor(private db:AngularFirestore) { }
}
