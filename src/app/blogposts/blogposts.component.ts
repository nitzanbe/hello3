import { Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { BlogpostsService } from '../blogposts.service';
import { ComonsService } from '../comons.service';
import { Blogposts } from '../interfaces/blogposts';
import { Comons } from '../interfaces/comons';

@Component({
  selector: 'app-blogposts',
  templateUrl: './blogposts.component.html',
  styleUrls: ['./blogposts.component.css']
})
export class BlogpostsComponent implements OnInit {

  posts$:Observable<Blogposts>
  comons$:Observable<Comons>
  userId:string;
  message:string;
  postID:string;
 
  constructor(private blogpostsService:BlogpostsService, private comonsService:ComonsService, public authService:AuthService) { }

  ngOnInit(): void {
  this.authService.getUser().subscribe(
    user => {
      this.userId = user.uid;
      console.log(this.userId);
      this.posts$ = this.blogpostsService.getPosts(); 
      this.comons$ = this.comonsService.getComons(); 
    }
  )
}

  add(id,title,body,ST){
    this.blogpostsService.addPosts(this.userId,id,title,body,ST); 
    this.postID=id;
    this.message="Saved"
  }


}
