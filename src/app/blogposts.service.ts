import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Blogposts } from './interfaces/blogposts';

@Injectable({
  providedIn: 'root'
})
export class BlogpostsService {

   
  private URL = "http://jsonplaceholder.typicode.com/posts";
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  constructor(private http: HttpClient, private db:AngularFirestore) { }

  getPosts():Observable<Blogposts>{
    return this.http.get<Blogposts>(this.URL); 
  }

  addPosts(userId:string,id:number,title:string,body:string,ST:number){
    const post = {id:id, title:title, body:body,ST:ST}; 
    this.userCollection.doc(userId).collection('posts').add(post);
  }



}
