import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../clients.service';
import { Client } from '../interfaces/client';
import { AuthService } from './../auth.service';

@Component({
  selector: 'clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients$;
  panelOpenState = false;
  userId:string;
  editstate = [];
  addClientFormOPen = false;
  

    
  constructor(public authService:AuthService, private clientsService:ClientsService, private predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.clients$ = this.clientsService.getClients(this.userId);
      }
    )
  }

  public deleteClient(id:string){
    this.clientsService.deleteClient(this.userId,id);
  }

  update(client:Client){
    this.clientsService.updateClient(this.userId ,client.id ,client.name, client.years, client.incame);
  }

  save(client:Client){
    this.clientsService.save(this.userId ,client.id ,client.name, client.years, client.incame ,client.category,client.ST=2);
  }

  add(client:Client){
    this.clientsService.addClient(this.userId,client.name,client.years,client.incame,client.category="",client.ST=0); 
  }


  public predict(client:Client){
    this.predictionService.predict(client.years,client.incame).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          client.category='YES';
          client.ST=1;
        } else {
          console.log('no');
          client.category='NO';
          client.ST=1;
        }
      }
    )
  }

  

}      
    

