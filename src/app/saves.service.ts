import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SavesService {

  saveCollection:AngularFirestoreCollection;  

  public getSaves(userId){
    this.saveCollection = this.db.collection(`users/${userId}/posts`);
    return this.saveCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  public deleteSave(userId:string , id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  like(userId:string,id:string,ST:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
        ST:ST
      }
    )
  }
  constructor(private db:AngularFirestore) { }
}
