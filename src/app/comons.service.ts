import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comons } from './interfaces/comons';

@Injectable({
  providedIn: 'root'
})
export class ComonsService {

  private URL = "http://jsonplaceholder.typicode.com/comments";
  
  constructor(private http: HttpClient) { }

  getComons():Observable<Comons>{
    return this.http.get<Comons>(this.URL); 
  }

}