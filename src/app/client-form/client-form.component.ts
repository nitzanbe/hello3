import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Client } from '../interfaces/client';

@Component({
  selector: 'client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
  @Input() name:string;
  @Input() years:number;
  @Input() incame:number;
  @Input() id:string; 
  @Output() update = new EventEmitter<Client>();
  @Output() closeEdit = new EventEmitter<null>();
  @Input() formType:string;
  isError:boolean = false;

  updateParent(){
    let client:Client = {id:this.id, name:this.name, years:this.years, incame:this.incame};
    if(this.years>24 || this.years<0){
      this.isError = true;
    }else{
      this.update.emit(client); 
      if(this.formType == "Add client"){
        this.name  = null;
        this.years = null;
        this.incame = null;
      }
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  constructor() { }

  ngOnInit(): void {
  }

}
